var app = new Vue({
  el: '#app',
  data: {
    modelName: null,
    fileStore: null,
    model: null
  },
  methods: {
    upload: function(){

      let formData = new FormData();

      formData.append("file", app.fileStore);
      formData.append("modelName", app.modelName);

      const config = {
          baseURL: 'http://35.245.234.120/',
          // headers: {
          //     'Content-Type': 'multipart/form-data',
          // }
      };

    axios.post('java', formData, config)
        .then(function (response) {
            app.model = response.data;
        })
        .catch(function (error) {
            app.model = error.message;
        })
    },

    processFile(event) {
      app.fileStore = event.target.files[0];
      console.log(app.fileStore);
    }
  }
})
